var express = require('express');
var router = express.Router();
var User = require('../models/user');
var passport = require('passport');
var verify = require('./verify');

/* Page : Homepage -- GET */
router.get('/', function(req, res, next) {
    res.json({
        message: "Nothing here!"
    })
});

/* Page : login -- POST */
router.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        req.logIn(user, function(err) {
            var token = verify.getToken(user);
            res.json({
                status: 'Success',
                informations: token
            });
        });
    })(req, res, next);
});

/* Page : register -- POST */
router.post('/register', function(req, res, next) {
    User.register(new User({
            username: req.body.username,
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            mail: req.body.mail,
            isAdmin: req.body.isadmin
        }),
        req.body.password,
        function(err, user) {
            if (err) {
                res.json({
                    status: 'Error',
                    informations: err
                });
            }
            res.json({
                status: 'Success',
                informations: user
            });
        }
    );
});

module.exports = router;
