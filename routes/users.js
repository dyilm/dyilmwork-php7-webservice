const express = require('express');
const router = express.Router();
const User = require('../models/user');
const passport = require('passport');
const verify = require('./verify');
const validator = require('validator');


var count = 0;


/* All page -- All methods  */
router.all('/*', verify.verifyUser, function(req, res, next) {
    next();
});

/* Page : List all users -- GET */
router.get('/', function(req, res, next) {
    User.find({}, function(err, users) {
        if (err) res.json({ status: 'Error', informations: err });
        res.json({ status: 'Success', informations: users });
    });
});

/* Page : List all students -- GET */
router.get('/students', function(req, res, next) {
    if (req.decoded._doc.role == 'trainer' || req.decoded._doc.role == 'administrator') {
        User.find({
            role: 'student'
        }, 'firstname lastname mail', function(err, users) {
            if (err) {
                displayError(1001, err, res);
            } else {
                res.json({ status: 'Success', informations: users });
            }
        });
    } else {
        displayError(1401, "You have no sufficient rights.", res);
    }
});

/* Page : List all trainers -- GET */
router.get('/trainers', function(req, res, next) {
    if (req.decoded._doc.role == 'administrator') {
        User.find({
            role: 'trainer'
        }, function(err, users) {
            if (err) {
                displayError(1001, err, res);
            } else {
                res.json({ status: 'Success', informations: users });
            }
        });
    } else {
        displayError(1401, "You have no sufficient rights.", res);
    }
});

/* Page: Show informations of current user -- GET */
router.get('/me', function(req, res, next) {
    if (req.decoded._doc.role == 'student' || req.decoded._doc.role == 'trainer' || req.decoded._doc.role == 'administrator') {
        User.findOne({
            mail: req.decoded._doc.mail
        }, function(err, user) {
            if (err) {
                displayError(1001, err, res);
            } else {
                res.json({ status: 'Success', informations: users });
            }
        });
    } else {
        displayError(1401, "You have no sufficient rights.", res);
    }
});

/* Page : Show one student -- GET */
router.get('/student/:mail', function(req, res, next) {
    if (req.decoded._doc.role == 'trainer') {
        if (validator.isEmail(req.params.mail)) {
            User.findOne({
                mail: req.params.mail
            }, function(err, user) {
                if (err) {
                    displayError(1001, err, res);
                } else if (user != null) {
                    if (user.role == 'student') res.json({ status: 'Success', informations: users });
                    else displayError(1003, null, res);
                } else {
                    displayError(1001, null, res);
                }
            });
        } else {
            displayError(1002, null, res);
        }
    } else {
        displayError(1401, "You have no sufficient rights.", res);
    }

});

/* Page : delete user -- DELETE */
router.delete('/:mail', function(req, res, next) {
    if (req.decoded._doc.role == 'administrator') {
        if (validator.isEmail(req.params.mail)) {
            User.findOneAndRemove({
                mail: req.params.mail
            }, function(err, user) {
                if (err) displayError(1001, err, res);
                else res.json({ status: "Sucess", informations: "Deleted" });
            });
        } else {
            displayError(1002, null, res);
        }
    } else {
        displayError(1401, "You have no sufficient rights.", res);
    }

});

/* Page : update user -- PATCH */
router.patch('/me/update', function(req, res, next) {
    if (req.decoded._doc.mail == req.body.mail) {
        User.findOneAndUpdate({
                mail: req.body.mail
            }, {
                $set: req.body
            }, {
                new: true
            },
            function(err, user) {
                if (err) {
                    displayError(1001, err, res);
                } else {
                    res.json({ status: 'Success', informations: users });
                }
            });
    } else {
        displayError(1401, "You have no sufficient rights.", res);
    }
}).patch('/update', function(req, res, next) {
    if (req.decoded._doc.role == 'administrator') {
        User.findOneAndUpdate({
                mail: req.body.mail
            }, {
                $set: req.body
            }, {
                new: true
            },
            function(err, user) {
                if (err) {
                    displayError(1001, err, res);
                } else {
                    res.json({ status: 'Success', informations: users });
                }
            });
    } else {
        displayError(1401, "You have no sufficient rights.", res);
    }
});

module.exports = router;

var displayError = function(errCode, data, res) {
    switch (errCode) {
        case 1401:
            JSONError = {
                status: 401,
                errorCode: errCode,
                message: "Unauthorized"
            };
            if (data != null) JSONError.informations = data;
            break;
        case 1001:
            JSONError = {
                status: 'Error',
                errorCode: errCode,
                message: "No users found"
            };
            if (data != null) JSONError.informations = data;
            break;
        case 1002:
            JSONError = {
                status: 'Error',
				errorCode: errCode,
                message: "No valid email address"
            };
            if (data != null) JSONError.informations = data;
            break;
		case 1003:
            JSONError = {
                status: 'Error',
				errorCode: errCode,
                message: "This user is not student"
            };
            if (data != null) JSONError.informations = data;
            break;
        default:
            JSONError = {
                status: 'Error'
            };
    }
    res.json(JSONError);
};
