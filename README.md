# NodeJS (PHP7) - Webservice

## Definition

- moyen rapide de distribution de l'information entre plusieurs personnes ou sites
- composant logiciel identifié par une URI
- fournissent un lien entre applications
- accessible via le réseau
- dispose d'une interface public

## Création d'un projet

1. Installation de node, npm, mongo & express
2. Avec un terminal, aller dans le dossier du projet
3. Executer les commandes :

  1. Init projet : `npm init`
  2. Installer mongoose & express-generator : `npm install mongoose express-generator --save`
  3. Créer le projet express : `express`
  4. Installer les dépandances : `npm install`
  5. Lancer le serveur : `npm start`

4. Lancer dans un naviguateur : <http://localhost:3000>

## Utilisation MongoDB

1. Installer MongoDB : <https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-16-04>
2. Lancer le service : `sudo systemctl start mongodb`
3. Verifier le statut: `sudo systemctl status mongodb`
4. Lancer le cli : `mongo`

Notes :

- Voir toutes les Base de données : `Show dbs`
- Rentrer dans une Base de données: `use {-Nom db-}`
- Voir toutes les tables : `show tables`
- Voir les entrées d'une table : `db.{-Nom table-}.find().pretty()`

## Notes

### Rôles utilisateur

- Etudiant : `student`
- Formateur : `trainer`
- Administrateur : `administrator`

### Routes disponible

URL                     | Description                                         | Methods
------------------------|-----------------------------------------------------|--------
`/login/`               | Connection de l'utilisateur [Retour : token]        | POST
`/register/`            | Inscription d'utilisateur                           | POST
`/users/`               | Liste tous les utilisateurs                         | GET
`/users/students/`      | Liste tous les étudiants                            | GET
`/users/trainers/`      | Liste tous les formateurs                           | GET
`/users/student/:mail/` | Affiche un étudiant                                 | GET
`/users/:mail/`         | Supprime un utilisateur                             | DELETE
`/users/me`             | Afficher les informations de l'utilisateur actuelle | GET
`/users/me/update`      | Modifier les informations de l'utilisateur actuelle | PATCH

## Liens

- Github formateur : <https://github.com/sheplu/nodeapi>
