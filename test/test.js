process.env.NODE_ENV = 'test';

const chai = require('chai');
const chai_http = require('chai-http');
const mongoose = require('mongoose');
const server = require('../app');
const User = require('../models/user');

var should = chai.should();
chai.use(chai_http);

var sampleUser = null;
var sampleUserToken = null;

describe('Test', function () {
    this.timeout(5000);
    User.collection.drop();
	beforeEach(function(done) {
		sampleUser = new User({
			'firstname': "John",
			'lastname': "Doe",
			"mail": "jd@gmail.com",
			'password': "aqwzsxedc"
		});
		sampleUser.save(function(err) {
			done();
		});
	});
	afterEach(function(done) {
		User.collection.drop();
		done();
	})
    it('should register user in /register POST', function (done) {
        // console.log(sampleUser);
        chai.request(server)
            .post('/register').send({
                'username': 'MG',
                'firstname': 'Mac',
                'lastname': 'Gyver',
                'mail': 'mgyver@exemple.com',
                'password': 'pass1324'
            }).end(function (err, res) {
                // console.log(res.body);
                res.should.have.status(200);
				res.should.be.json;
				res.body.should.be.a('object');
                res.body.should.have.property('status', 'Success');
				res.body.should.have.deep.property('informations.username');
				done();
            })
    });
    it('should looged user in /login POST', function (done) {
        // console.log(sampleUser);
        chai.request(server)
            .post('/login').send({
                'username': sampleUser.username,
                'password': sampleUser.password
            }).end(function (err, res) {
                // console.log(res.body);
                res.should.have.status(200);
				res.should.be.json;
				res.body.should.be.a('object');
                res.body.should.have.property('status', 'Success');

                if (res.body.informations != null) {
                    sampleUserToken = res.body.informations;
                }
                console.log(sampleUserToken);
				done();
            })
    });
    it('should list all users in /users/ GET', function (done) {
        chai.request(server)
        .get('/users/')
        .set('x-access-token', sampleUserToken)
        .end(function (err, res) {
            console.log(res.body);

            res.should.have.status(200);
            res.should.be.json;
            res.body.should.have.property('status', 'Success');

            done();
        });
    });
})
