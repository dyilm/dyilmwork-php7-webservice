var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var pass_local_mongoose = require('passport-local-mongoose');

var User = new Schema({
    username: String,
    firstname: String,
    lastname: String,
    mail: {
        type: String,
        unique: true
    },
    password: String,
    role: {
        type: String,
        default: 'student'
    }
});

User.plugin(pass_local_mongoose);
module.exports = mongoose.model('User', User);
