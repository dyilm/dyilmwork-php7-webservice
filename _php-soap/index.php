<?php
/**
 * Index Page
 */

if($_SERVER['REQUEST_METHOD'] === 'POST'){

    if(isset($_POST['tablelength'])){
        $client = new SoapClient('http://footballpool.dataaccess.eu/data/info.wso?wsdl');
        $topScorers = $client->TopGoalScorers([
            'iTopN' => $_POST['tablelength']
        ]);
        $topScorers = $topScorers->TopGoalScorersResult->tTopGoalScorer;
        // var_dump($topScorers);

    }else{
        $topScorers = null;
    }
}else{
    $topScorers = null;
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- CSS -->
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="container"><div class="row">

        <form class="" action="./" method="post">
            <div class="form-group">
                <label for="tablelength">Nombre de joueurs&nbsp;:&nbsp;</label>
                <input type="number" min="2" max="20" name="tablelength" id="tablelength">
            </div>
            <button type="submit" class="btn btn-default">Envoyer</button>
        </form>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Top buts</th>
                    <th>Pays</th>
                </tr>
            </thead>
            <tbody>
                <?php if($topScorers == null){ ?>
                        <tr>
                            <td colspan="3">Sélectionner le nombre de joueurs</td>
                        </tr>
                <?php
                    } else {
                        foreach ($topScorers as $key => $value) {
                ?>
                            <tr>
                                <td><?= $value->sName ?></td>
                                <td><?= $value->iGoals ?></td>
                                <td><img src="<?= $value->sFlag ?>" alt="Flag"></td>
                            </tr>
                <?php
                        }
                    }
                ?>
            </tbody>
        </table>
    </div>
    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
